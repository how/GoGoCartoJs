# 1.4.2
* BUG: Fixs images in compactJson (used in marker popup template)
* BUG: Fixs broken showOnePanePerMainOption

# 1.4.1
* MINOR documentation now accessible on [gitlab pages](https://pixelhumain.gitlab.io/GoGoCartoJs)
* FEATURE: Custom templates for Marker Popup
* FEATURE: Display number of element for each category
* MINOR: Improve UI (theme, fontawesome icons)

#1.4
* Creation of this changelog
